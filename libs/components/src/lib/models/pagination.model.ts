export interface Pagination {
  currentPage: number;
  total: number;
  perPageCount: number;
  pageCount: number;
}
